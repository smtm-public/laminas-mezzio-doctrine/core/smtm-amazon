<?php

declare(strict_types=1);

namespace Smtm\Amazon;

use Smtm\Base\Infrastructure\Helper\EnvHelper;

if (file_exists(__DIR__ . '/../../../../.env.smtm.smtm-amazon')) {
    $dotenv = \Dotenv\Dotenv::createMutable(
        __DIR__ . '/../../../../',
        '.env.smtm.smtm-amazon'
    );
    $dotenv->load();
}

return [
    'aws' => [
        's3' => [
            'client' =>
                json_decode(
                    EnvHelper::getEnvFromProcessOrSuperGlobal(
                        [
                            'SMTM_AMAZON_AWS_S3_CLIENT',
                        ],
                        '[]'
                    ),
                    true
                ),
        ]
    ]
];
