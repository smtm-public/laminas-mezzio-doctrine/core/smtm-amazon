<?php

declare(strict_types=1);

namespace Smtm\Amazon;

use Aws\S3\S3ClientInterface;
use Smtm\Amazon\Infrastructure\Service\S3\S3Client;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Amazon\Infrastructure\Service\S3\Factory\S3ClientFactory;
use Psr\Container\ContainerInterface;

return [
    'delegators' => [
        InfrastructureServicePluginManager::class => [
            function (
                ContainerInterface $container,
                $name,
                callable $callback,
                array $options = null
            ) {
                /** @var InfrastructureServicePluginManager $infrastructureServicePluginManager */
                $infrastructureServicePluginManager = $callback();

                return $infrastructureServicePluginManager->configure(
                    [
                        'factories' => [
                            S3Client::class => S3ClientFactory::class,
                        ],
                        'aliases' => [
                            S3ClientInterface::class => S3Client::class,
                        ],
                    ]
                );
            }
        ],
    ],
];
