<?php

declare(strict_types=1);

namespace Smtm\Amazon;

use JetBrains\PhpStorm\ArrayShape;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class ConfigProvider
{
    #[ArrayShape(['dependencies' => 'array', 'amazon' => 'array'])] public function __invoke(): array
    {
        return [
            'dependencies' => include __DIR__ . '/../config/dependencies.php',
            'amazon' => include __DIR__ . '/../config/amazon.php',
        ];
    }
}
