<?php

declare(strict_types=1);

namespace Smtm\Amazon\Factory;

use Smtm\Base\Factory\ConfigAwareDelegator;

class AmazonConfigAwareDelegator extends ConfigAwareDelegator
{
    protected array|string|null $configKey = 'amazon';
}
