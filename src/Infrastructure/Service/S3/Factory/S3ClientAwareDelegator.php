<?php

declare(strict_types=1);

namespace Smtm\Amazon\Infrastructure\Service\S3\Factory;

use Smtm\Amazon\Infrastructure\Service\S3\S3Client;
use Smtm\Amazon\Infrastructure\Service\S3\S3ClientAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareInterface;
use Smtm\Base\Factory\ServiceNameAwareTrait;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Laminas\ServiceManager\Factory\DelegatorFactoryInterface;
use Psr\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class S3ClientAwareDelegator implements DelegatorFactoryInterface, ServiceNameAwareInterface
{

    use ServiceNameAwareTrait;

    public function __invoke(ContainerInterface $container, $name, callable $callback, array $options = null)
    {
        /** @var S3ClientAwareInterface $object */
        $object = $callback();

        $object->setS3Client(
            $container
                ->get(InfrastructureServicePluginManager::class)
                ->get($options['name'] ?? $this->getServiceName() ?? S3Client::class)
        );

        return $object;
    }
}
