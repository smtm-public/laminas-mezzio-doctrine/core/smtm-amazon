<?php

declare(strict_types=1);

namespace Smtm\Amazon\Infrastructure\Service\S3\Factory;

use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;
use Smtm\Amazon\Infrastructure\Service\S3\S3Client;
use Interop\Container\ContainerInterface;

/**
 * @author Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class S3ClientFactory
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new S3Client(
            $container->get(InfrastructureServicePluginManager::class),
            $options ?? $container->get('config')['amazon']['aws']['s3']['client']
        );
    }
}
