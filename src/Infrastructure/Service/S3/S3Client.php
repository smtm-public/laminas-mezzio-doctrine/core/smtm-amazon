<?php

declare(strict_types=1);

namespace Smtm\Amazon\Infrastructure\Service\S3;

use Smtm\Base\Infrastructure\Service\InfrastructureServiceInterface;
use Smtm\Base\Infrastructure\Service\InfrastructureServicePluginManager;

/**
 * Milen Dzhambazov <savemetenminutes@gmail.com>
 */
class S3Client extends \Aws\S3\S3Client implements InfrastructureServiceInterface
{
    public function __construct(
        protected InfrastructureServicePluginManager $infrastructureServicePluginManager,
        array $args
    ) {
        parent::__construct($args);
    }
}
