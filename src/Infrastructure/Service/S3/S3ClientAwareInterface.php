<?php

declare(strict_types=1);

namespace Smtm\Amazon\Infrastructure\Service\S3;

interface S3ClientAwareInterface
{
    public function getS3Client(): S3Client;
    public function setS3Client(S3Client $s3Client): static;
}
